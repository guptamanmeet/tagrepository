﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;


namespace TagsExecutor
{
    class TagsCommand
    {
        private const String cTftTagsFolder = "C:\\TFTPL\\TAGS\\BIN\\";
        private const String cTftTagsExe = cTftTagsFolder + "TAGS.exe";
        private const String cTagsIniFile = cTftTagsFolder + "Parameters.ini";
        private const String cTagsIniTestSuiteLabel = "TestSuite";
        private const String cTagsIniObjectRpositoryLabel = "ORLocation";
        private const String cTagsResultFolderLabel = "LogDestinationFolder";
        private const String cTagsResultFolder = "\\Result";

        private static String getValue(String label)
        {
            string[] lines = File.ReadAllLines(cTagsIniFile);
            foreach (string line in lines)
            {
                string[] strKeyValue = line.Split(':');
                if (strKeyValue[0] == label)
                {
                    return strKeyValue[1];
                }
            }

            return String.Empty;
        }

        private static bool setValue(String label, String value)
        {
            String[] lines = File.ReadAllLines(cTagsIniFile);
            Boolean found = false;
            for (int i = 0; i < lines.Length; i++)
            {
                String[] strKeyValue = lines[i].Split(':');
                if (label.Equals(strKeyValue[0], StringComparison.InvariantCultureIgnoreCase))
                {
                    lines[i] = strKeyValue[0] + ": " + value;
                    found = true;
                    break;
                }
            }

            if (found)
            {

                File.WriteAllLines(cTagsIniFile, lines);
            }

            return found;
        }

        private static void updateTagsConfig(String testSuite, String objectRepository, String output)
        {
            setValue(cTagsIniTestSuiteLabel, testSuite);
            setValue(cTagsIniObjectRpositoryLabel, objectRepository);
            setValue(cTagsResultFolderLabel, output);
        }
        public static void run(String testSuiteFolder, String testSuite, String objectRepository)
        {
            updateTagsConfig(testSuiteFolder + testSuite,
                             testSuiteFolder,
                             testSuiteFolder + cTagsResultFolder);

            // remove earlier artifacts if present
            if (Directory.Exists(testSuiteFolder + cTagsResultFolder))
            {
                Directory.Delete(testSuiteFolder + cTagsResultFolder, true);
            }

            Process process = new Process();
            ProcessStartInfo info = new ProcessStartInfo("cmd.exe", "/C" + cTftTagsExe);
            process.StartInfo = info;
            process.Start();
            process.WaitForExit();
        }
    }
}

