﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace TagsExecutor
{
    class TagsResultAggregator
    {

        private const String cTagsTestSuiteFolder = "C:\\TagsTestData\\";
        private const String cTagsOutputFolder = cTagsTestSuiteFolder + "Output\\";
        private const String cResultFileName = "_Result.xml";

        public static void aggregate(String testSuiteName)
        {

                XmlDocument myDestDoc = new XmlDocument();
                XmlDeclaration xmlDeclaration = myDestDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlElement rootElement = myDestDoc.CreateElement("TestCases");
                myDestDoc.AppendChild(rootElement);
                foreach (string file in Directory.EnumerateFiles(cTagsOutputFolder, "*.xml", SearchOption.AllDirectories))
                {
                    createXmlDoc(myDestDoc, rootElement, file);
                }

            myDestDoc.Save(cTagsOutputFolder + testSuiteName + "\\" + testSuiteName + cResultFileName);
        }

            static void createXmlDoc(XmlDocument myDestDoc, XmlElement rootElement, String testSuiteName)
            {

            XmlTextReader myReader = new XmlTextReader(testSuiteName);
            XmlDocument mySourceDoc = new XmlDocument();
            mySourceDoc.Load(myReader);
            myReader.Close();

            String fullPath = Path.GetFullPath(testSuiteName).TrimEnd(Path.DirectorySeparatorChar);
            String projectName = Path.GetDirectoryName(fullPath);
            projectName = Path.GetDirectoryName(projectName);
            projectName = Path.GetFileName(projectName);
            String[] projectLabel = projectName.Split('-');

            XmlElement element = myDestDoc.CreateElement(projectLabel[0]);
            rootElement.AppendChild(element);
            myReader = new XmlTextReader(cTagsOutputFolder);
            XmlNode rootDest = myDestDoc.GetElementsByTagName(projectLabel[0])[0];
            XmlNode nodeOrig = mySourceDoc["NewDataSet"];                                     // Store the node to be copied into an XmlNode  .ChildNodes[1]
            XmlNodeList testSteps = nodeOrig.SelectNodes("TestStep");
            foreach (XmlNode n in testSteps)
            {
                XmlNode nodeDest = myDestDoc.ImportNode(n, true);
                if (null != nodeDest)
                {
                    rootDest.AppendChild(nodeDest);                                          // Append the node being copied to the root of the destination document
                }
            }
        }
    }
}
