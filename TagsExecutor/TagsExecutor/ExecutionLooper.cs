﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace TagsExecutor
{
    class ExecutionLooper
    {

        private const String cTagsInputDrive = "C:\\";
        private const String cTagsInputFolder = cTagsInputDrive + "TagsTestData\\";
        private const String cTagsRequestFolder = cTagsInputFolder + "Requests\\";
        private const String cTagsOutputFolder = cTagsInputFolder + "Output\\";
        private const String cTestSuiteResultFolder = "\\Result";

        private enum STATES { IDLE = 0, INIT, EXECUTE, EXECUTING, STOP, STOPPING, STOPPED = 99 };

        private const int SLEEP_TIME = 1000;


        // this method waits for the requests to run execution on Tags.
        // The expecation is that the request files are kept in a folder that will be picked one by one to be executed.

        private STATES currentState = STATES.IDLE;
        private String currentRequestId = String.Empty;
        private long currentTagsProcessId = 0;


        private String getStateStr(STATES s)
        {
            String currentStateStr = String.Empty;
            switch (currentState)
            {
                case STATES.IDLE:
                    currentStateStr = "IDLE";
                    break;
                case STATES.INIT:
                    currentStateStr = "INIT";
                    break;
                case STATES.EXECUTE:
                    currentStateStr = "EXECUTE";
                    break;
                case STATES.EXECUTING:
                    currentStateStr = "EXECUTING";
                    break;
                case STATES.STOP:
                    currentStateStr = "STOP";
                    break;
                case STATES.STOPPED:
                    currentStateStr = "STOPPED";
                    break;
                case STATES.STOPPING:
                    currentStateStr = "STOPPING";
                    break;
                default:
                    currentStateStr = "UNKNOWN";
                    break;
            }
            return currentStateStr;
        }


        private void log(String message)
        {
            Console.WriteLine("STATE = " + getStateStr(currentState) + ": " + message);
        }


        private void checkIfProcessRunning()
        {
            // if the TAGS.EXE is stopped running then 
            //if (!isTagsProcessRunning())
            //{
            currentState = STATES.STOP;
            //}
            // else do nothing
        }

        private void completeRequestExecution()
        {
            // remove the request file with .start prefix.
            log("Request execution completed");
            try
            {
                changeRequestFileState(currentState);
            }
            catch (Exception ex)
            {
                log(ex.Message);
            }
            currentState = STATES.IDLE;
            log("ready to take new requests");
        }

        private void checkForRequests()
        {
            // pick next request
            currentRequestId = getNextRequest();
            if (String.Empty != currentRequestId)
            {
                // accept the request
                log("Request Id = " + currentRequestId);
                currentState = STATES.INIT;
            }
            else
            {
                log("No Request");
            }
        }

        private void executeTagsTestSuite()
        {
            if (!startFileNameExists())
            {
                log(currentRequestId + ".start file does not exist");
                currentState = STATES.IDLE;
            }
            try
            {
                currentState = STATES.EXECUTING;
                changeRequestFileState(currentState);
                log("Execution has been started");
                startTestSuiteExecutionOnTags();
            }
            catch (Exception ex)
            {
                log(ex.Message);
                currentState = STATES.IDLE;
            }
        }

        private void changeRequestFileStateToError()
        {
            if (File.Exists(cTagsOutputFolder + currentRequestId + ".start"))
            {
                File.Move(cTagsOutputFolder + currentRequestId + ".start",
                        cTagsOutputFolder + currentRequestId + ".error");
            }
            else if (File.Exists(cTagsOutputFolder + currentRequestId + ".executing"))
            {
                File.Move(cTagsOutputFolder + currentRequestId + ".executing",
                        cTagsOutputFolder + currentRequestId + ".error");
            }
        }

        private void changeRequestFileState(STATES currentState)
        {
            if (currentState == STATES.EXECUTING)
            {
                File.Move(cTagsOutputFolder + currentRequestId + ".start",
                    cTagsOutputFolder + currentRequestId + ".executing");
            }
            else if (currentState == STATES.STOPPED)
            {
                log("Checking for executing file " + cTagsOutputFolder + currentRequestId + ".executing");
                if (File.Exists(cTagsOutputFolder + currentRequestId + ".executing"))
                {
                    if (File.Exists(cTagsOutputFolder + currentRequestId + ".done"))
                    {
                        File.Delete(cTagsOutputFolder + currentRequestId + ".done");
                    }
                    File.Move(cTagsOutputFolder + currentRequestId + ".executing",
                        cTagsOutputFolder + currentRequestId + ".done");
                }
            }
        }

        private void prepareForRequestExecution()
        {
            // set start in the file request file name
            // Rename the file to postfix .start to currentRequestId filename
            if (String.Empty != currentRequestId)
            {
                currentState = STATES.EXECUTE;
                renameFileToStartExecution();
                log("Ready to start execution");
            }
            else
            {
                currentState = STATES.IDLE;
            }
        }

        private void renameFileToStartExecution()
        {

            String fileName = currentRequestId;
            log("NAME OF FILE  " + fileName);
            if (!File.Exists(cTagsRequestFolder + fileName) || !Directory.Exists(cTagsRequestFolder + 
                                                                        Path.GetFileNameWithoutExtension(fileName)))
            {
                log("Request File or Request Directory does not exist : " + currentRequestId);
                currentState = STATES.IDLE;
                return;
            }

            try
            {
                log("Request file and Folder found. request = " + fileName);

                // remove the .start file from output as we are still single threaded.
                if (File.Exists(cTagsOutputFolder + fileName + ".start"))
                {
                    File.Delete(cTagsOutputFolder + fileName + ".start");
                }
                File.Move(cTagsRequestFolder + fileName, cTagsOutputFolder + fileName + ".start");
                log(cTagsRequestFolder + fileName + " moved to " + cTagsOutputFolder + fileName + ".start");
                if (Directory.Exists(cTagsOutputFolder + Path.GetFileNameWithoutExtension(fileName)))
                {
                    Directory.Delete(cTagsOutputFolder + Path.GetFileNameWithoutExtension(fileName), true);
                }
                Directory.Move(cTagsRequestFolder + Path.GetFileNameWithoutExtension(fileName),
                                                                    cTagsOutputFolder + Path.GetFileNameWithoutExtension(fileName));

                log(cTagsRequestFolder + Path.GetFileNameWithoutExtension(fileName) +
                                        " moved to " + cTagsOutputFolder + Path.GetFileNameWithoutExtension(fileName));
                currentState = STATES.EXECUTE;
            }
            catch (Exception ex)
            {
                log(ex.Message);
                changeRequestFileStateToError();
                currentState = STATES.IDLE;
            }

        }

        private String getResultFolder()
        {
            try
            {
                String resultFolder = cTagsOutputFolder + Path.GetFileNameWithoutExtension(currentRequestId) + cTestSuiteResultFolder;
                log(resultFolder);
                return Directory.Exists(resultFolder) ? resultFolder : String.Empty;
            }
            catch (Exception ex)
            {
                log(ex.Message);
                return String.Empty;
            }
        }

        private void aggregateTestResults()
        {
            try
            {
                TagsResultAggregator.aggregate(Path.GetFileNameWithoutExtension(currentRequestId));
            }
            catch (Exception e)
            {
                log("Result aggregation failed. " + e.Message);
            } 

        }

        private void stopRequestExecution()
        {
            log("Stopping request execution");
            currentState = STATES.STOPPING;
            String resultFolder = getResultFolder();
            if (String.Empty == resultFolder)
            {
                currentState = STATES.IDLE;
                return;
            }
            try
            {
                aggregateTestResults(); // this is empty but can be filled if required
                log("Stopping execution and copying result to " + resultFolder);
            }
            catch (Exception ex)
            {
                log(ex.Message);
                currentState = STATES.IDLE;
            }

        }

        private Boolean isTagsProcessRunning()
        {
            return false;
        }

        private void startTestSuiteExecutionOnTags()
        {
            String testSuiteFolder = cTagsOutputFolder + Path.GetFileNameWithoutExtension(currentRequestId);
            TagsCommand.run(testSuiteFolder,
                                "\\TestSuite.xls",
                            "\\ObjectRepository.xls");
        }

        private Boolean startFileNameExists()
        {
            try
            {
                return (File.Exists(cTagsOutputFolder + currentRequestId + ".start"));
            }
            catch (Exception ex)
            {
                log(ex.Message);
                return false;
            }
        }


        private String getNextRequest()
        {
            FileInfo oldest = null;
            DirectoryInfo folder;
            try
            {
                folder = new DirectoryInfo(cTagsRequestFolder);

                foreach (FileInfo file in folder.GetFiles())
                {
                    if (oldest == null || file.LastWriteTime < oldest.LastWriteTime)
                    {
                        oldest = file;
                    }
                }

                if (oldest != null)
                    return oldest.Name;

            }
            catch (Exception ex)
            {
                log(ex.Message);
            }

            return String.Empty;
        }
  
  
        public void run()
        {
            // for now keep the 
            while (true)
            {
                switch (currentState)
                {
                    case STATES.IDLE:
                        checkForRequests();
                        break;
                    case STATES.INIT:
                        prepareForRequestExecution();
                        break;
                    case STATES.EXECUTE:
                        // run the Tags executable
                        executeTagsTestSuite();
                        break;
                    case STATES.EXECUTING:
                        checkIfProcessRunning();
                        break;
                    case STATES.STOP:
                        stopRequestExecution();
                        break;
                    case STATES.STOPPING:
                        // do nothing
                        currentState = STATES.STOPPED;
                        break;
                    case STATES.STOPPED:
                        completeRequestExecution();
                        break;
                    default:
                        Console.WriteLine("Should never come here");
                        break;
                }

                Thread.Sleep(SLEEP_TIME);
            }
        }
    }
}